/*
 * Copyright (c) 2015-2016 Beijing HuntingX Technology Co.,Ltd. All Rights Reserved.
 */
package com.hunting4x.p4x;

import org.apache.http.entity.ContentType;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.ByteArrayBody;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.entity.mime.content.InputStreamBody;
import org.apache.http.entity.mime.content.StringBody;

import java.io.File;
import java.io.InputStream;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Random;

/**
 * Http Multipart<br />
 *
 * @author h
 * @since 1.0.0
 * @version 1.3.0
 */
public class PostParameters {
	private MultipartEntity multiPart = null;
	private final static int boundaryLength = 32;
	private final static String boundaryAlphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789_";
	private String boundary;
	
	/**
	 * auto generate boundary string
	 * @return a boundary string
	 */
	private String getBoundary() {
		StringBuilder sb = new StringBuilder();
		Random random = new Random();
		for (int i = 0; i < boundaryLength; ++i)
			sb.append(boundaryAlphabet.charAt(random.nextInt(boundaryAlphabet.length())));
		return sb.toString();
	}
	
	/**
	 * @return get MultipartEntity (apache)
	 */
	public MultipartEntity getMultiPart() {
		return multiPart;
	}
	
	/**
	 * default boundary is auto generate {@link #getBoundary()}
	 */
	public PostParameters() {
		super();
		boundary = getBoundary();
		multiPart = new MultipartEntity(HttpMultipartMode.STRICT , boundary,  Charset.forName("UTF-8"));
	}
	
	/**
	 * @return multipart boundary string
	 */
	public String boundaryString() {
		return boundary;
	}
	
	/**
	 * 设置访问的URL
     *
	 * @param url 访问的URL
	 * @return this
	 */
	public PostParameters setUrl(String url){
        multiPart.addPart("url", new StringBody(url, ContentType.create("text/plain", "UTF-8")));
		return this;
	}
	
	/**
     * 设置表单属性
     * <p>
     *     `attr`=`value`
     * </p>
	 *
	 * @param attr key
     * @param value 字符串值
	 * @return this
	 */
	public PostParameters addAttribute(String attr, String value) {
        multiPart.addPart(attr, new StringBody(value, ContentType.create("text/plain", "UTF-8")));
		return this;
	}
	
	/**
     * 设置表单属性
     * <p>
     *     `attr`=`value`
     * </p>
     *
     * @param attr key
     * @param file 文件类型值
     * @return this
     */
    public PostParameters addAttribute(String attr, File file) {
        multiPart.addPart(attr, new FileBody(file));
        return this;
    }
    
    /**
     * 设置表单属性
     * <p>
     *     `attr`=`value`
     * </p>
     *
     * @param attr key
     * @param buffer 字节数组
     * @return this
     */
    public PostParameters addAttribute(String attr, byte[] buffer) {
        multiPart.addPart(attr, new ByteArrayBody(buffer, attr));
        return this;
    }

	/**
	 * 设置表单属性
     * <p>
     *     `attr`=`value`
     * </p>
     *
	 * @param attr key
     * @param inputStream 流
	 * @return this
	 */
	public PostParameters addAttribute(String attr, InputStream inputStream) {
		multiPart.addPart(attr, new InputStreamBody(inputStream, attr));
		return this;
	}
	
	private String toStringList(String[] sa) {
		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < sa.length; ++i) {
			if (i != 0) sb.append(',');
			sb.append(sa[i]);
		}
		
		return sb.toString();
	}
	
	private String toStringList(ArrayList<String> sa) {
		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < sa.size(); ++i) {
			if (i != 0) sb.append(',');
			sb.append(sa.get(i));
		}
		
		return sb.toString();
	}
}
