/*
 * Copyright (c) 2015-2016 Beijing HuntingX Technology Co.,Ltd. All Rights Reserved.
 */
package com.hunting4x.error;

public class RestResponseParseException extends Exception {
	
	private String errorMessage = null;
	private String errorCode = null;
	private Integer httpResponseCode = null;
	
	private static final long serialVersionUID = 3;

	public RestResponseParseException(String message) {
		super(message);
	}
	
	public RestResponseParseException(String message, String errorCode, String errorMessage, int httpResponseCode) {
		super(message + " code="+errorCode + ", message="+errorMessage + ", responseCode="+httpResponseCode);
		this.errorCode = errorCode;
		this.errorMessage = errorMessage;
		this.httpResponseCode = httpResponseCode;
	}

	public String getErrorMessage() {
		return errorMessage;
	}

	public String getErrorCode() {
		return errorCode;
	}

	public Integer getHttpResponseCode() {
		return httpResponseCode;
	}
	
	public boolean isAPIError() {
		return this.errorCode != null && this.errorMessage != null && this.httpResponseCode != null; 
	}
		
}

