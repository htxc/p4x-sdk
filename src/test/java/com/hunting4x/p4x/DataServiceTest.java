/*
 * Copyright (c) 2015-2016 Beijing HuntingX Technology Co.,Ltd. All Rights Reserved.
 */

package com.hunting4x.p4x;

import org.json.JSONObject;

import java.io.File;
import java.io.FileInputStream;

/**
 * 数据服务测试类
 *
 * @author zhangws
 */
public class DataServiceTest {

    public static final String url_server = "http://service.huntingid.cn:9006/p4x/";

    public static final String app_id = "app_id";
    public static final String app_key = "app_key";

    public static void main(String[] args) throws Exception {
        //testIdentify();

        testVerify();
    }

    public static void testIdentify() throws Exception {
        JSONObject result;
        HttpRequests request = new HttpRequests(url_server, app_id, app_key);

        PostParameters params = new PostParameters();
        params.addAttribute("personalName", "姓名");
        params.addAttribute("identityCardNo", "身份证号");
        result = request.request("data", "identify", params);
        String code = result.getString("code");
        String msg = result.getString("msg");

        System.out.println("identify：code = " + code + " msg = " + msg);
    }

    public static void testVerify() throws Exception {
        JSONObject result;
        HttpRequests request = new HttpRequests(url_server, app_id, app_key);

        PostParameters params = new PostParameters();
        params.addAttribute("personalName", "姓名");
        params.addAttribute("identityCardNo", "身份证号");
//        params.addAttribute("collectPhoto", new File("/Users/test.jpg"));
        params.addAttribute("collectPhoto", new FileInputStream(new File("/Users/test.jpg")));
        result = request.request("data", "verify", params);
        String code = result.getString("code");
        String msg = result.getString("msg");

        System.out.println("verify：code = " + code + " msg = " + msg);
    }

}
